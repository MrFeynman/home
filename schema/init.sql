CREATE EXTENSION "uuid-ossp";

CREATE TYPE public."communications" AS ENUM (
	'heating',
	'sewerage',
	'electricity',
	'network',
	'ventilation');

CREATE TYPE public."person_status" AS ENUM (
	'owner',
	'roommate',
	'guest',
	'tenant');

CREATE TABLE public.person (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"name" varchar NOT NULL,
	age int2 NOT NULL,
	CONSTRAINT person_pk PRIMARY KEY (id)
);

CREATE TABLE public.house (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	address varchar NOT NULL,
	"type" varchar NOT NULL,
	"comment" varchar NULL,
	CONSTRAINT house_pkey PRIMARY KEY (id)
);

CREATE TABLE public.appartment (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	house_id uuid NOT NULL,
	"number" int2 NOT NULL,
	"comment" varchar NULL,
	owner_id uuid NULL,
	CONSTRAINT appartment_pk PRIMARY KEY (id),
	CONSTRAINT appartment_fk FOREIGN KEY (owner_id) REFERENCES public.person(id) ON DELETE SET NULL,
	CONSTRAINT appartment_house_fk FOREIGN KEY (house_id) REFERENCES public.house(id) ON DELETE CASCADE
);

CREATE TABLE public.room (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	appartment_id uuid NOT NULL,
	"comment" varchar NULL,
	"communications" public."_communications" NULL,
	reserved_free_area float4 NULL,
	height float4 NOT NULL,
	width float4 NOT NULL,
	"depth" float4 NOT NULL,
	CONSTRAINT room_pk PRIMARY KEY (id),
	CONSTRAINT room_appartment_fk FOREIGN KEY (appartment_id) REFERENCES public.appartment(id)
);

CREATE TABLE public.lodger (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	appartment_id uuid NOT NULL,
	person_id uuid NOT NULL,
	status public."person_status" NULL,
	CONSTRAINT lodger_pk PRIMARY KEY (id),
	CONSTRAINT lodger_appartment_fk FOREIGN KEY (appartment_id) REFERENCES public.appartment(id),
	CONSTRAINT lodger_person_fk FOREIGN KEY (person_id) REFERENCES public.person(id)
);

CREATE TABLE public.property (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	owner_id uuid NULL,
	room_id uuid NULL,
	"name" varchar NOT NULL,
	description varchar NULL,
	"comment" varchar NULL,
	height float4 NULL DEFAULT 0,
	width float4 NULL DEFAULT 0,
	"depth" float4 NULL DEFAULT 0,
	need_communications public."_communications" NULL,
	CONSTRAINT furniture_pk PRIMARY KEY (id),
	CONSTRAINT property_person_fk FOREIGN KEY (owner_id) REFERENCES public.person(id) ON DELETE SET NULL,
	CONSTRAINT property_room_fk FOREIGN KEY (room_id) REFERENCES public.room(id)
);
