package internal

import (
	"context"
	"house_api/internal/routers/appartment"
	"house_api/internal/routers/enums"
	"house_api/internal/routers/house"
	"house_api/internal/routers/lodger"
	"house_api/internal/routers/person"
	"house_api/internal/routers/property"
	"house_api/internal/routers/room"
	"house_api/pkg/external/postgres"
	"house_api/pkg/settings"
	"log"
	"net/http"
	"os"

	httpSwagger "github.com/swaggo/http-swagger"

	"github.com/gorilla/mux"
)

type App struct {
	db     postgres.DBIface
	router *mux.Router
}

type NewHandler interface {
	RegisterRouter(*mux.Router)
	InitService(postgres.DBIface)
}

func (app *App) AddHandler(new_handler NewHandler) {
	new_handler.RegisterRouter(app.router)
	new_handler.InitService(app.db)
}

func CreateApp(ctx context.Context) (*App, error) {
	settings := settings.GetPGSettings()
	pool, err := postgres.InitPool(ctx, settings)

	if err != nil {
		return nil, err
	}

	mux := mux.NewRouter()

	app := &App{
		db:     pool,
		router: mux,
	}

	app.AddHandler(person.NewHandler())
	app.AddHandler(lodger.NewHandler())
	app.AddHandler(house.NewHandler())
	app.AddHandler(appartment.NewHandler())
	app.AddHandler(room.NewHandler())
	app.AddHandler(property.NewHandler())
	app.AddHandler(enums.NewHandler())

	app.router.HandleFunc("/swagger/doc.json", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/x-yaml")
		data, err := os.ReadFile("openapi.yaml")
		if err != nil {
			log.Printf("openapi.yaml err   #%v ", err)
		}
		_, _ = w.Write(data)

		w.WriteHeader(http.StatusOK)
	}).Methods(http.MethodGet)

	app.router.PathPrefix("/swagger").Handler(httpSwagger.WrapHandler)
	return app, nil
}

func (app *App) Run() {
	log.Fatal(http.ListenAndServe(":8000", app.router))
	defer app.db.Close()
}
