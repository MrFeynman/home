package room

import (
	"github.com/google/uuid"
)

type RoomInDB struct {
	ID               uuid.UUID `json:"_id" validate:"required"`
	AppartmentID     uuid.UUID `json:"appartment_id" validate:"required"`
	Comment          *string   `json:"comment"`
	Communications   *[]string `json:"communications" validate:"oneof=heating sewerage electricity network ventilation"`
	ReservedFreeArea *float32  `json:"reserved_free_area"`
	Width            float32   `json:"width" validate:"required"`
	Heigh            float32   `json:"height" validate:"required"`
	Depth            float32   `json:"depth" validate:"required"`
}

type CreateRoomInput struct {
	AppartmentID     uuid.UUID `json:"appartment_id" validate:"required"`
	Comment          *string   `json:"comment"`
	Communications   *[]string `json:"communications" validate:"oneof=heating sewerage electricity network ventilation"`
	ReservedFreeArea *float32  `json:"reserved_free_area"`
	Width            float32   `json:"width" validate:"required"`
	Heigh            float32   `json:"height" validate:"required"`
	Depth            float32   `json:"depth" validate:"required"`
}

type UpdateRoomInput struct {
	Comment          *string   `json:"comment"`
	Communications   *[]string `json:"communications" validate:"oneof=heating sewerage electricity network ventilation"`
	ReservedFreeArea *float32  `json:"reserved_free_area"`
}

type UpdateRoomService struct {
	ID               uuid.UUID `json:"_id" validate:"required"`
	Comment          *string   `json:"comment"`
	Communications   *[]string `json:"communications" validate:"oneof=heating sewerage electricity network ventilation"`
	ReservedFreeArea *float32  `json:"reserved_free_area"`
}

func (room *RoomInDB) Area() float32 {
	return room.Depth * room.Width
}
