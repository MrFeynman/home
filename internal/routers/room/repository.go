package room

import (
	"context"
	"fmt"
	"house_api/pkg/external/postgres"
	"house_api/pkg/helpers"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

type Repository struct {
	db postgres.DBIface
}

func NewRepository(db postgres.DBIface) *Repository {
	return &Repository{db: db}
}

func (r *Repository) Create(ctx context.Context, input CreateRoomInput) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		INSERT INTO room
			(
				appartment_id,
				comment,
				communications,
				reserved_free_area,
				width,
				height,
				depth
			)
		VALUES
			($1, $2, $3, $4, $5, $6, $7)
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.AppartmentID,
		input.Comment,
		input.Communications,
		input.ReservedFreeArea,
		input.Width,
		input.Heigh,
		input.Depth,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetOne(ctx context.Context, uid uuid.UUID) (*RoomInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, appartment_id, comment, communications, reserved_free_area, width, height, depth
		FROM room
		WHERE id = $1
	`
	var result RoomInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		uid,
	)

	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(
			&result.ID,
			&result.AppartmentID,
			&result.Comment,
			&result.Communications,
			&result.ReservedFreeArea,
			&result.Width,
			&result.Heigh,
			&result.Depth,
		)
		if err != nil {
			return nil, err
		}
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetOneOrder: %v", err)
	}
	return &result, nil
}

func (r *Repository) Update(ctx context.Context, input UpdateRoomService) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		UPDATE room
		SET comment = $1,
			communications = $2,
			reserved_free_area = $3
		WHERE id = $4
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.Comment,
		input.Communications,
		input.ReservedFreeArea,
		input.ID,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetAll(ctx context.Context, skip int, limit int) (*[]RoomInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, appartment_id, comment, communications, reserved_free_area, width, height, depth
		FROM room
		ORDER BY appartment_id
		LIMIT $1
		OFFSET $2
	`
	var results []RoomInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		limit,
		skip,
	)

	defer rows.Close()
	for rows.Next() {
		room := RoomInDB{}
		err := rows.Scan(
			&room.ID,
			&room.AppartmentID,
			&room.Comment,
			&room.Communications,
			&room.ReservedFreeArea,
			&room.Width,
			&room.Heigh,
			&room.Depth,
		)
		if err != nil {
			return nil, err
		}
		results = append(results, room)
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetAllOrder: %v", err)
	}
	return &results, nil
}

func (r *Repository) Delete(ctx context.Context, uid uuid.UUID) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		DELETE
		FROM room
		WHERE id = $1
		RETURNING id
	`

	var deleted_id *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		uid,
	).Scan(&deleted_id); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetOneOrder: %v", err)
	}
	return deleted_id, nil
}
