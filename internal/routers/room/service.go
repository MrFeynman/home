package room

import (
	"context"
	"house_api/pkg/external/postgres"

	"github.com/google/uuid"
)

type ServiceInt interface {
	Create(appartment CreateRoomInput) (int, error)
	GetAll(house_id uuid.UUID) ([]RoomInDB, error)
	GetById(appartment_id uuid.UUID) (RoomInDB, error)
	Delete(appartment_id uuid.UUID) error
	Update(appartment_id uuid.UUID, input UpdateRoomInput) error
}

type Service struct {
	repository *Repository
}

func NewService(db postgres.DBIface) *Service {
	return &Service{repository: NewRepository(db)}
}

func (s *Service) Create(ctx context.Context, input CreateRoomInput) (*uuid.UUID, error) {
	return s.repository.Create(ctx, input)
}

func (s *Service) GetOne(
	ctx context.Context,
	uid uuid.UUID,
) (*RoomInDB, error) {
	return s.repository.GetOne(ctx, uid)
}

func (s *Service) Update(
	ctx context.Context,
	input UpdateRoomInput,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	service_input := UpdateRoomService{
		ID:               uid,
		Comment:          input.Comment,
		Communications:   input.Communications,
		ReservedFreeArea: input.ReservedFreeArea,
	}
	return s.repository.Update(ctx, service_input)
}

func (s *Service) GetAll(ctx context.Context, skip int, limit int) (*[]RoomInDB, error) {
	return s.repository.GetAll(ctx, skip, limit)
}

func (s *Service) Delete(
	ctx context.Context,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	return s.repository.Delete(ctx, uid)
}
