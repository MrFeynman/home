package house

import (
	"context"
	"fmt"
	"house_api/internal/routers/appartment"
	"house_api/pkg/external/postgres"
	"house_api/pkg/helpers"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

type Repository struct {
	db postgres.DBIface
}

func NewRepository(db postgres.DBIface) *Repository {
	return &Repository{db: db}
}

func (r *Repository) Create(ctx context.Context, input CreateHouseInput) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		INSERT INTO house
			(address, type, comment)
		VALUES
			($1, $2, $3)
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.Address, input.Type, input.Comment,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetOne(ctx context.Context, uid uuid.UUID) (*HouseInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, address, type, comment
		FROM house
		WHERE id = $1
	`
	var result HouseInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		uid,
	)

	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(
			&result.ID,
			&result.Address,
			&result.Type,
			&result.Comment,
		)
		if err != nil {
			return nil, err
		}
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetOneOrder: %v", err)
	}
	return &result, nil
}

func (r *Repository) Update(ctx context.Context, input UpdateHouseService) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		UPDATE house
		SET comment = $1
		WHERE id = $2
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.Comment,
		input.ID,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetAll(ctx context.Context, skip int, limit int) (*[]HouseInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, address, type, comment
		FROM house
		ORDER BY address
		LIMIT $1
		OFFSET $2
	`
	var results []HouseInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		limit,
		skip,
	)

	defer rows.Close()
	for rows.Next() {
		house := HouseInDB{}
		err := rows.Scan(
			&house.ID,
			&house.Address,
			&house.Type,
			&house.Comment,
		)
		if err != nil {
			return nil, err
		}
		results = append(results, house)
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetAllOrder: %v", err)
	}
	return &results, nil
}

func (r *Repository) Delete(ctx context.Context, uid uuid.UUID) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		DELETE
		FROM house
		WHERE id = $1
		RETURNING id
	`

	var deleted_id *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		uid,
	).Scan(&deleted_id); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("DeleteOrder: %v", err)
	}
	return deleted_id, nil
}

func (r *Repository) GetAppartments(
	ctx context.Context,
	uid uuid.UUID,
	skip int,
	limit int,
) (*[]appartment.ApartmentInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, house_id, number, comment, owner_id
		FROM appartment
		WHERE house_id = $1
		LIMIT $2
		OFFSET $3
	`
	var results []appartment.ApartmentInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		uid,
		limit,
		skip,
	)

	defer rows.Close()
	for rows.Next() {
		apart := appartment.ApartmentInDB{}
		err := rows.Scan(
			&apart.ID,
			&apart.HouseID,
			&apart.Number,
			&apart.Comment,
			&apart.OwnerId,
		)
		if err != nil {
			return nil, err
		}
		results = append(results, apart)
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetAllOrder: %v", err)
	}
	return &results, nil
}
