package house

import "github.com/google/uuid"

type HouseInDB struct {
	ID      uuid.UUID `json:"_id" validate:"required"`
	Address string    `json:"address" validate:"required"`
	Type    string    `json:"type" validate:"required"`
	Comment *string   `json:"comment"`
}

type CreateHouseInput struct {
	Address string  `json:"address" validate:"required"`
	Type    string  `json:"type" validate:"required"`
	Comment *string `json:"comment"`
}

type UpdateHouseInput struct {
	Comment *string `json:"comment"`
}

type UpdateHouseService struct {
	ID      uuid.UUID `json:"_id" validate:"required"`
	Comment *string   `json:"comment"`
}
