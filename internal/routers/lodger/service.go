package lodger

import (
	"context"
	"house_api/pkg/external/postgres"

	"github.com/google/uuid"
)

type ServiceInt interface {
	Create(appartment CreateLodgerInput) (int, error)
	GetAll(house_id uuid.UUID) ([]LodgerInDB, error)
	GetById(appartment_id uuid.UUID) (LodgerInDB, error)
	Delete(appartment_id uuid.UUID) error
	Update(appartment_id uuid.UUID, input UpdateLodgerInput) error
}

type Service struct {
	repository *Repository
}

func NewService(db postgres.DBIface) *Service {
	return &Service{repository: NewRepository(db)}
}

func (s *Service) Create(ctx context.Context, input CreateLodgerInput) (*uuid.UUID, error) {
	return s.repository.Create(ctx, input)
}

func (s *Service) GetOne(
	ctx context.Context,
	uid uuid.UUID,
) (*LodgerInDB, error) {
	return s.repository.GetOne(ctx, uid)
}

func (s *Service) Update(
	ctx context.Context,
	input UpdateLodgerInput,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	service_input := UpdateLodgerService{
		AppartmentID: input.AppartmentID,
		Status:       input.Status,
		ID:           uid,
	}
	return s.repository.Update(ctx, service_input)
}

func (s *Service) GetAll(ctx context.Context, skip int, limit int) (*[]LodgerInDB, error) {
	return s.repository.GetAll(ctx, skip, limit)
}

func (s *Service) Delete(
	ctx context.Context,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	return s.repository.Delete(ctx, uid)
}
