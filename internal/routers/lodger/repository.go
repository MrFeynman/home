package lodger

import (
	"context"
	"fmt"
	"house_api/pkg/external/postgres"
	"house_api/pkg/helpers"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

type Repository struct {
	db postgres.DBIface
}

func NewRepository(db postgres.DBIface) *Repository {
	return &Repository{db: db}
}

func (r *Repository) Create(ctx context.Context, input CreateLodgerInput) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		INSERT INTO lodger
			(appartment_id, person_id, status)
		VALUES
			($1, $2, $3)
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.AppartmentID, input.PersonID, input.Status,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetOne(ctx context.Context, uid uuid.UUID) (*LodgerInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, appartment_id, person_id, status
		FROM lodger
		WHERE id = $1
	`
	var result LodgerInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		uid,
	)

	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(
			&result.ID,
			&result.AppartmentID,
			&result.PersonID,
			&result.Status,
		)
		if err != nil {
			return nil, err
		}
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetOneOrder: %v", err)
	}
	return &result, nil
}

func (r *Repository) Update(ctx context.Context, input UpdateLodgerService) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		UPDATE lodger
		SET appartment_id = $1,
			status = $2
		WHERE id = $3
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.AppartmentID,
		input.Status,
		input.ID,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetAll(ctx context.Context, skip int, limit int) (*[]LodgerInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, appartment_id, person_id, status
		FROM lodger
		ORDER BY appartment_id
		LIMIT $1
		OFFSET $2
	`
	var results []LodgerInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		limit,
		skip,
	)

	defer rows.Close()
	for rows.Next() {
		lodger := LodgerInDB{}
		err := rows.Scan(
			&lodger.ID,
			&lodger.AppartmentID,
			&lodger.PersonID,
			&lodger.Status,
		)
		if err != nil {
			return nil, err
		}
		results = append(results, lodger)
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetAllOrder: %v", err)
	}
	return &results, nil
}

func (r *Repository) Delete(ctx context.Context, uid uuid.UUID) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		DELETE
		FROM lodger
		WHERE id = $1
		RETURNING id
	`

	var deleted_id *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		uid,
	).Scan(&deleted_id); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetOneOrder: %v", err)
	}
	return deleted_id, nil
}
