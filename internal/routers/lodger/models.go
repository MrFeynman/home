package lodger

import "github.com/google/uuid"

type LodgerInDB struct {
	ID           uuid.UUID `json:"_id" validate:"required"`
	AppartmentID uuid.UUID `json:"appartment_id" validate:"required "`
	PersonID     uuid.UUID `json:"person_id" validate:"required "`
	Status       string    `json:"status" validate:"required "`
}

type CreateLodgerInput struct {
	AppartmentID uuid.UUID `json:"appartment_id" validate:"required "`
	PersonID     uuid.UUID `json:"person_id" validate:"required "`
	Status       string    `json:"status" validate:"required "`
}

type UpdateLodgerInput struct {
	AppartmentID uuid.UUID `json:"appartment_id" validate:"required "`
	Status       string    `json:"status" validate:"required "`
}

type UpdateLodgerService struct {
	ID           uuid.UUID `json:"_id" validate:"required"`
	AppartmentID uuid.UUID `json:"appartment_id" validate:"required "`
	Status       string    `json:"status" validate:"required "`
}
