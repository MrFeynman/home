package appartment

import (
	"github.com/google/uuid"
)

type ApartmentInDB struct {
	ID      uuid.UUID `json:"_id" validate:"required"`
	HouseID uuid.UUID `json:"house_id" validate:"required"`
	Number  uint16    `json:"number" validate:"required"`
	Comment *string   `json:"comment"`
	OwnerId *string   `json:"owner_id"`
}

type CreateApartmentInput struct {
	HouseID uuid.UUID `json:"house_id" validate:"required"`
	Number  uint16    `json:"number" validate:"required"`
	Comment *string   `json:"comment"`
	OwnerId *string   `json:"owner_id"`
}

type UpdateApartmentInput struct {
	Comment *string `json:"comment"`
}

type UpdateApartmentService struct {
	ID      uuid.UUID `json:"_id" validate:"required"`
	Comment *string   `json:"comment"`
}
