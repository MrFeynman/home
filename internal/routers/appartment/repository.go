package appartment

import (
	"context"
	"fmt"
	"house_api/pkg/external/postgres"
	"house_api/pkg/helpers"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

type Repository struct {
	db postgres.DBIface
}

func NewRepository(db postgres.DBIface) *Repository {
	return &Repository{db: db}
}

func (r *Repository) Create(ctx context.Context, input CreateApartmentInput) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		INSERT INTO appartment
			(house_id, number, comment, owner_id)
		VALUES
			($1, $2, $3, $4)
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.HouseID, input.Number, input.Comment, input.OwnerId,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetOne(ctx context.Context, uid uuid.UUID) (*ApartmentInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, house_id, number, comment, owner_id
		FROM appartment
		WHERE id = $1
	`
	var result ApartmentInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		uid,
	)

	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(
			&result.ID,
			&result.HouseID,
			&result.Number,
			&result.Comment,
			&result.OwnerId,
		)
		if err != nil {
			return nil, err
		}
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetOneOrder: %v", err)
	}
	return &result, nil
}

func (r *Repository) Update(ctx context.Context, input UpdateApartmentService) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		UPDATE appartment
		SET comment = $1
		WHERE id = $2
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.Comment,
		input.ID,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("UpdateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("UpdateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetAll(ctx context.Context, skip int, limit int) (*[]ApartmentInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, house_id, number, comment, owner_id
		FROM appartment
		ORDER BY house_id
		LIMIT $1
		OFFSET $2
	`
	var results []ApartmentInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		limit,
		skip,
	)

	defer rows.Close()
	for rows.Next() {
		apart := ApartmentInDB{}
		err := rows.Scan(
			&apart.ID,
			&apart.HouseID,
			&apart.Number,
			&apart.Comment,
			&apart.OwnerId,
		)
		if err != nil {
			return nil, err
		}
		results = append(results, apart)
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetAllOrder: %v", err)
	}
	return &results, nil
}

func (r *Repository) Delete(ctx context.Context, uid uuid.UUID) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		DELETE
		FROM appartment
		WHERE id = $1
		RETURNING id
	`

	var deleted_id *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		uid,
	).Scan(&deleted_id); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetOneOrder: %v", err)
	}
	return deleted_id, nil
}
