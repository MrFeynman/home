package appartment

import (
	"context"
	"house_api/pkg/external/postgres"

	"github.com/google/uuid"
)

type ServiceInt interface {
	Create(appartment CreateApartmentInput) (int, error)
	GetAll(house_id uuid.UUID) ([]ApartmentInDB, error)
	GetById(appartment_id uuid.UUID) (ApartmentInDB, error)
	Delete(appartment_id uuid.UUID) error
	Update(appartment_id uuid.UUID, input UpdateApartmentInput) error
}

type Service struct {
	repository *Repository
}

func NewService(db postgres.DBIface) *Service {
	return &Service{repository: NewRepository(db)}
}

func (s *Service) Create(ctx context.Context, input CreateApartmentInput) (*uuid.UUID, error) {
	return s.repository.Create(ctx, input)
}

func (s *Service) GetOne(
	ctx context.Context,
	uid uuid.UUID,
) (*ApartmentInDB, error) {
	return s.repository.GetOne(ctx, uid)
}

func (s *Service) Update(
	ctx context.Context,
	input UpdateApartmentInput,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	service_input := UpdateApartmentService{
		Comment: input.Comment,
		ID:      uid,
	}
	return s.repository.Update(ctx, service_input)
}

func (s *Service) GetAll(ctx context.Context, skip int, limit int) (*[]ApartmentInDB, error) {
	return s.repository.GetAll(ctx, skip, limit)
}

func (s *Service) Delete(
	ctx context.Context,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	return s.repository.Delete(ctx, uid)
}
