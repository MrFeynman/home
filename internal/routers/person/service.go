package person

import (
	"context"
	"house_api/pkg/external/postgres"

	"github.com/google/uuid"
)

type ServiceInt interface {
	Create(appartment CreatePersonInput) (int, error)
	GetAll(house_id uuid.UUID) ([]PersonInDB, error)
	GetById(appartment_id uuid.UUID) (PersonInDB, error)
	Delete(appartment_id uuid.UUID) error
	Update(appartment_id uuid.UUID, input UpdatePersonInput) error
}

type Service struct {
	repository *Repository
}

func NewService(db postgres.DBIface) *Service {
	return &Service{repository: NewRepository(db)}
}

func (s *Service) Create(ctx context.Context, input CreatePersonInput) (*uuid.UUID, error) {
	return s.repository.Create(ctx, input)
}

func (s *Service) GetOne(
	ctx context.Context,
	uid uuid.UUID,
) (*PersonInDB, error) {
	return s.repository.GetOne(ctx, uid)
}

func (s *Service) Update(
	ctx context.Context,
	input UpdatePersonInput,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	service_input := UpdatePersonService{
		Age:  input.Age,
		Name: input.Name,
		ID:   uid,
	}
	return s.repository.Update(ctx, service_input)
}

func (s *Service) GetAll(ctx context.Context, skip int, limit int) (*[]PersonInDB, error) {
	return s.repository.GetAll(ctx, skip, limit)
}

func (s *Service) Delete(
	ctx context.Context,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	return s.repository.Delete(ctx, uid)
}
