package person

import "github.com/google/uuid"

type PersonInDB struct {
	ID   uuid.UUID `json:"_id" validate:"required"`
	Name string    `json:"name" validate:"required"`
	Age  int       `json:"age" validate:"required gte=0 lte=130"`
}

type CreatePersonInput struct {
	Name string `json:"name" validate:"required"`
	Age  int    `json:"age" validate:"required gte=0 lte=130"`
}

type UpdatePersonInput struct {
	Name string `json:"name" validate:"required"`
	Age  int    `json:"age" validate:"required gte=0 lte=130"`
}

type UpdatePersonService struct {
	ID   uuid.UUID `json:"_id" validate:"required"`
	Name string    `json:"name" validate:"required"`
	Age  int       `json:"age" validate:"required gte=0 lte=130"`
}
