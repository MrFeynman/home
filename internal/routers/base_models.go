package routers

import "github.com/google/uuid"

type CreatedInfo struct {
	CreatedID uuid.UUID `json:"created_id"`
}

type DeletedInfo struct {
	DeletedID uuid.UUID `json:"updated_id"`
}

type UpdatedInfo struct {
	UpdatedID uuid.UUID `json:"updated_id"`
}

type CreateResponse struct {
	Status string      `json:"status"`
	Data   CreatedInfo `json:"message"`
}

type UpdateResponse struct {
	Status string      `json:"status"`
	Data   UpdatedInfo `json:"message"`
}

type DeletedResponse struct {
	Status string      `json:"status"`
	Data   DeletedInfo `json:"message"`
}
