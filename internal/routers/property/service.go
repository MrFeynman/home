package property

import (
	"context"
	"house_api/pkg/external/postgres"

	"github.com/google/uuid"
)

type ServiceInt interface {
	Create(appartment CreatePropertyInput) (int, error)
	GetAll(house_id uuid.UUID) ([]PropertyInDB, error)
	GetById(appartment_id uuid.UUID) (PropertyInDB, error)
	Delete(appartment_id uuid.UUID) error
	Update(appartment_id uuid.UUID, input UpdatePropertyInput) error
}

type Service struct {
	repository *Repository
}

func NewService(db postgres.DBIface) *Service {
	return &Service{repository: NewRepository(db)}
}

func (s *Service) Create(ctx context.Context, input CreatePropertyInput) (*uuid.UUID, error) {
	return s.repository.Create(ctx, input)
}

func (s *Service) GetOne(
	ctx context.Context,
	uid uuid.UUID,
) (*PropertyInDB, error) {
	return s.repository.GetOne(ctx, uid)
}

func (s *Service) Update(
	ctx context.Context,
	input UpdatePropertyInput,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	service_input := UpdatePropertyService{
		ID:                 uid,
		OwnerId:            input.OwnerId,
		RoomId:             input.RoomId,
		NeedCommunications: input.NeedCommunications,
		Name:               input.Name,
		Description:        input.Description,
		Comments:           input.Comments,
	}
	return s.repository.Update(ctx, service_input)
}

func (s *Service) GetAll(ctx context.Context, skip int, limit int) (*[]PropertyInDB, error) {
	return s.repository.GetAll(ctx, skip, limit)
}

func (s *Service) Delete(
	ctx context.Context,
	uid uuid.UUID,
) (*uuid.UUID, error) {
	return s.repository.Delete(ctx, uid)
}
