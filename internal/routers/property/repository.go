package property

import (
	"context"
	"fmt"
	"house_api/pkg/external/postgres"
	"house_api/pkg/helpers"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

type Repository struct {
	db postgres.DBIface
}

func NewRepository(db postgres.DBIface) *Repository {
	return &Repository{db: db}
}

func (r *Repository) Create(ctx context.Context, input CreatePropertyInput) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		INSERT INTO property
			(
				owner_id,
				room_id,
				need_communications,
				name,
				description,
				comment,
				width,
				height,
				depth
			)
		VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9)
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.OwnerId,
		input.RoomId,
		input.NeedCommunications,
		input.Name,
		input.Description,
		input.Comments,
		input.Width,
		input.Heigh,
		input.Depth,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetOne(ctx context.Context, uid uuid.UUID) (*PropertyInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, owner_id, room_id, need_communications, name, description, comment, width, height, depth
		FROM property
		WHERE id = $1
	`
	var result PropertyInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		uid,
	)

	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(
			&result.ID,
			&result.OwnerId,
			&result.RoomId,
			&result.NeedCommunications,
			&result.Name,
			&result.Description,
			&result.Comment,
			&result.Width,
			&result.Heigh,
			&result.Depth,
		)
		if err != nil {
			return nil, err
		}
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetOneOrder: %v", err)
	}
	return &result, nil
}

func (r *Repository) Update(ctx context.Context, input UpdatePropertyService) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback(ctx)

	query := `
		UPDATE property
		SET owner_id = $1,
			room_id = $2,
			need_communications = $3,
			name = $4,
			description = $5,
			comment = $6
		WHERE id = $7
		RETURNING id
	`

	var uid *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		input.OwnerId,
		input.RoomId,
		input.NeedCommunications,
		input.Name,
		input.Description,
		input.Comments,
		input.ID,
	).Scan(&uid); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if uid == nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("CreateOrder: %v", err)
	}

	return uid, nil
}

func (r *Repository) GetAll(ctx context.Context, skip int, limit int) (*[]PropertyInDB, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT id, owner_id, room_id, need_communications, name, description, comment, width, height, depth
		FROM property
		ORDER BY name
		LIMIT $1
		OFFSET $2
	`
	var results []PropertyInDB

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
		limit,
		skip,
	)

	defer rows.Close()
	for rows.Next() {
		property := PropertyInDB{}
		err := rows.Scan(
			&property.ID,
			&property.OwnerId,
			&property.RoomId,
			&property.NeedCommunications,
			&property.Name,
			&property.Description,
			&property.Comment,
			&property.Width,
			&property.Heigh,
			&property.Depth,
		)
		if err != nil {
			return nil, err
		}
		results = append(results, property)
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetAllOrder: %v", err)
	}
	return &results, nil
}

func (r *Repository) Delete(ctx context.Context, uid uuid.UUID) (*uuid.UUID, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		DELETE
		FROM property
		WHERE id = $1
		RETURNING id
	`

	var deleted_id *uuid.UUID

	if err = tx.QueryRow(
		ctx,
		helpers.FormatQuery(query),
		uid,
	).Scan(&deleted_id); err != nil {
		if err == pgx.ErrNoRows {
			return &uuid.Nil, nil
		}
		return nil, err
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetOneOrder: %v", err)
	}
	return deleted_id, nil
}
