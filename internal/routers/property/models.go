package property

import "github.com/google/uuid"

type PropertyInDB struct {
	ID                 uuid.UUID  `json:"_id" required:"true"`
	OwnerId            *uuid.UUID `json:"owner_id"`
	RoomId             *uuid.UUID `json:"room_id"`
	NeedCommunications *[]string  `json:"need_communications" validate:"oneof=heating sewerage electricity network ventilation"`
	Name               *string    `json:"name"`
	Description        *string    `json:"description"`
	Comment            *string    `json:"comment"`
	Width              *float32   `json:"width" validate:"required"`
	Heigh              *float32   `json:"height" validate:"required"`
	Depth              *float32   `json:"depth" validate:"required"`
}

type CreatePropertyInput struct {
	OwnerId            *uuid.UUID `json:"owner_id"`
	RoomId             *uuid.UUID `json:"room_id"`
	NeedCommunications *[]string  `json:"need_communications" validate:"oneof=heating sewerage electricity network ventilation"`
	Name               *string    `json:"name"`
	Description        *string    `json:"description"`
	Comments           *string    `json:"comments"`
	Width              *float32   `json:"width"`
	Heigh              *float32   `json:"height"`
	Depth              *float32   `json:"depth"`
}

type UpdatePropertyInput struct {
	OwnerId            *uuid.UUID `json:"owner_id"`
	RoomId             *uuid.UUID `json:"room_id"`
	NeedCommunications *[]string  `json:"need_communications" validate:"oneof=heating sewerage electricity network ventilation"`
	Name               *string    `json:"name"`
	Description        *string    `json:"description"`
	Comments           *string    `json:"comments"`
}

type UpdatePropertyService struct {
	ID                 uuid.UUID  `json:"_id" required:"true"`
	OwnerId            *uuid.UUID `json:"owner_id"`
	RoomId             *uuid.UUID `json:"room_id"`
	NeedCommunications *[]string  `json:"need_communications" validate:"oneof=heating sewerage electricity network ventilation"`
	Name               *string    `json:"name"`
	Description        *string    `json:"description"`
	Comments           *string    `json:"comments"`
}
