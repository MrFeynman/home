package property

import (
	"context"
	"encoding/json"
	"house_api/internal/routers"
	"house_api/pkg/external/postgres"
	"net/http"
	"strconv"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

type Handler struct {
	service *Service
}

func NewHandler() *Handler {
	return &Handler{
		service: nil,
	}
}

func (h *Handler) InitService(db postgres.DBIface) {
	h.service = NewService(db)
}

func (h *Handler) RegisterRouter(mux *mux.Router) {
	house_router := mux.PathPrefix("/property").Subrouter()
	house_router.HandleFunc("/create", h.Create).Methods(http.MethodPost)
	house_router.HandleFunc("/all/{skip}/{limit}", h.GetAll).Methods(http.MethodGet)
	house_router.HandleFunc("/{uuid}", h.GetOne).Methods(http.MethodGet)
	house_router.HandleFunc("/{uuid}", h.Update).Methods(http.MethodPatch)
	house_router.HandleFunc("/{uuid}", h.Delete).Methods(http.MethodDelete)
}

func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	var input CreatePropertyInput
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		r.Header.Set("Content-Type", "application/json")
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Wrong input\"}"))
		return
	}
	ctx := context.Background()
	uid, err := h.service.Create(ctx, input)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(routers.CreateResponse{Status: "ok", Data: routers.CreatedInfo{CreatedID: *uid}})
}

func (h *Handler) GetOne(w http.ResponseWriter, r *http.Request) {
	r.Header.Set("Content-Type", "application/json")
	get_uuid, err := uuid.Parse(mux.Vars(r)["uuid"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Wrong uuid\"}"))
		return
	}
	ctx := context.Background()
	result, err := h.service.GetOne(ctx, get_uuid)
	if result.ID == uuid.Nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Not Found\"}"))
		return
	}
	json.NewEncoder(w).Encode(result)
}

func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	var input UpdatePropertyInput
	r.Header.Set("Content-Type", "application/json")
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Wrong input\"}"))
		return
	}
	update_uuid, err := uuid.Parse(mux.Vars(r)["uuid"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Wrong uuid\"}"))
		return
	}
	ctx := context.Background()
	uid, err := h.service.Update(ctx, input, update_uuid)
	if *uid == uuid.Nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Not Found\"}"))
		return
	}
	json.NewEncoder(w).Encode(routers.UpdateResponse{Status: "ok", Data: routers.UpdatedInfo{UpdatedID: *uid}})
}

func (h *Handler) GetAll(w http.ResponseWriter, r *http.Request) {
	r.Header.Set("Content-Type", "application/json")
	skip, err := strconv.Atoi(mux.Vars(r)["skip"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Wrong skip\"}"))
		return
	}
	limit, err := strconv.Atoi(mux.Vars(r)["limit"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Wrong limit\"}"))
		return
	}
	ctx := context.Background()
	results, err := h.service.GetAll(ctx, skip, limit)
	json.NewEncoder(w).Encode(results)
}

func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	r.Header.Set("Content-Type", "application/json")
	del_uuid, err := uuid.Parse(mux.Vars(r)["uuid"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Wrong uuid\"}"))
		return
	}
	ctx := context.Background()
	deleted_uuid, err := h.service.Delete(ctx, del_uuid)
	if *deleted_uuid == uuid.Nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("{\"status\": \"error\", \"message\": \"Not Found\"}"))
		return
	}
	json.NewEncoder(w).Encode(routers.DeletedResponse{Status: "ok", Data: routers.DeletedInfo{DeletedID: *deleted_uuid}})
}
