package enums

import (
	"context"
	"encoding/json"
	"house_api/pkg/external/postgres"
	"net/http"

	"github.com/gorilla/mux"
)

type Handler struct {
	service *Service
}

func NewHandler() *Handler {
	return &Handler{
		service: nil,
	}
}

func (h *Handler) InitService(db postgres.DBIface) {
	h.service = NewService(db)
}

func (h *Handler) RegisterRouter(mux *mux.Router) {
	house_router := mux.PathPrefix("/enum").Subrouter()
	house_router.HandleFunc("/communications", h.GetCommunications).Methods(http.MethodGet)
	house_router.HandleFunc("/lodger_statuses", h.GetStatuses).Methods(http.MethodGet)
}

func (h *Handler) GetCommunications(w http.ResponseWriter, r *http.Request) {
	r.Header.Set("Content-Type", "application/json")
	ctx := context.Background()
	results, err := h.service.GetCommunications(ctx)
	if err != nil {
		w.WriteHeader(http.StatusTeapot)
		w.Write([]byte("{\"status\": \"error\"}"))
		return
	}
	json.NewEncoder(w).Encode(results)
}

func (h *Handler) GetStatuses(w http.ResponseWriter, r *http.Request) {
	r.Header.Set("Content-Type", "application/json")
	ctx := context.Background()
	results, err := h.service.GetStatuses(ctx)
	if err != nil {
		w.WriteHeader(http.StatusTeapot)
		w.Write([]byte("{\"status\": \"error\"}"))
		return
	}
	json.NewEncoder(w).Encode(results)
}
