package enums

import (
	"context"
	"house_api/pkg/external/postgres"
)

type Service struct {
	repository *Repository
}

func NewService(db postgres.DBIface) *Service {
	return &Service{repository: NewRepository(db)}
}

func (s *Service) GetCommunications(
	ctx context.Context,
) (*[]string, error) {
	return s.repository.GetCommunications(ctx)
}

func (s *Service) GetStatuses(
	ctx context.Context,
) (*[]string, error) {
	return s.repository.GetStatuses(ctx)
}
