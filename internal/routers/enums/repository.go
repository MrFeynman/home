package enums

import (
	"context"
	"fmt"
	"house_api/pkg/external/postgres"
	"house_api/pkg/helpers"
)

type Repository struct {
	db postgres.DBIface
}

func NewRepository(db postgres.DBIface) *Repository {
	return &Repository{db: db}
}

func (r *Repository) GetCommunications(ctx context.Context) (*[]string, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT unnest(enum_range(NULL::communications))
	`
	var results []string

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
	)

	defer rows.Close()
	for rows.Next() {
		var lodger_type string
		err := rows.Scan(
			&lodger_type,
		)
		if err != nil {
			return nil, err
		}
		results = append(results, lodger_type)
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetAllOrder: %v", err)
	}
	return &results, nil
}

func (r *Repository) GetStatuses(ctx context.Context) (*[]string, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	query := `
		SELECT unnest(enum_range(NULL::person_status))
	`
	var results []string

	rows, err := tx.Query(
		ctx,
		helpers.FormatQuery(query),
	)

	defer rows.Close()
	for rows.Next() {
		var lodger_type string
		err := rows.Scan(
			&lodger_type,
		)
		if err != nil {
			return nil, err
		}
		results = append(results, lodger_type)
	}
	if err = tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("GetAllOrder: %v", err)
	}
	return &results, nil
}
