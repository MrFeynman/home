ok_start:
	sudo docker compose up -d --build

start: ok_start
	clear
	sudo docker ps -a

ok_stop:
	sudo docker compose down

stop: ok_stop
	sudo docker system prune

restart: ok_stop start

