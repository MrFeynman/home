# House Service

## Requirements

### Docker Compose

[Install Docker](https://docs.docker.com/engine/install/ubuntu/)

[Install Docker Compose plugin](https://docs.docker.com/compose/install/linux/)

## Usage

Start Server

```bash
make start
```

Stop Server

```bash
make stop
```

Restart Server

```bash
make restart
```

Base Endpoints

|name|url|
-|-
|swagger|<http://localhost:8000/swagger/index.html>|
|pgadmin|<http://localhost:8080>|
|postgres connection|<http://localhost:5432>|

*Replace host and port according to your own settings

## Letteratura utile

- [Standard Go Project Layout](https://github.com/golang-standards/project-layout)

- [Best practices for containerizing Go applications with Docker](<https://snyk.io/blog/containerizing-go-applications-with-docker/>)
