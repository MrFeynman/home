package main

import (
	"context"
	"house_api/internal"
)

func main() {
	ctx := context.Background()
	app, err := internal.CreateApp(ctx)
	if err != nil {
		return
	}
	app.Run()
}
