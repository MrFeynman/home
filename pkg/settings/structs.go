package settings

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type PostgresSettings struct {
	Username string
	Password string
	Host     string
	Port     string
	Database string
}

func GetPostgressSettings() *PostgresSettings {
	err := godotenv.Load()
	if err != nil {
		log.Print("No .env file")
	}
	pg_settings = &PostgresSettings{
		Username: os.Getenv("postgress.user"),
		Password: os.Getenv("postgress.password"),
		Host:     os.Getenv("postgress.host"),
		Port:     os.Getenv("postgress.port"),
		Database: os.Getenv("postgress.database"),
	}
	return pg_settings
}
