package settings

import (
	"sync"
)

var (
	pg_settings *PostgresSettings
	once        sync.Once
)

func GetPGSettings() *PostgresSettings {
	once.Do(func() {
		pg_settings = GetPostgressSettings()
	})

	return pg_settings
}
