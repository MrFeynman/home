package postgres

import (
	"context"
	"fmt"
	"log"
	"time"

	"house_api/pkg/helpers"
	"house_api/pkg/settings"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
)

type DBIface interface {
	Begin(ctx context.Context) (pgx.Tx, error)
	Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	Ping(ctx context.Context) error
	Close()
}

func InitPool(
	ctx context.Context,
	settings *settings.PostgresSettings,
) (pool *pgxpool.Pool, err error) {
	dsn := fmt.Sprintf(
		"postgresql://%s:%s@%s:%s/%s",
		settings.Username,
		settings.Password,
		settings.Host,
		settings.Port,
		settings.Database,
	)
	err = helpers.DoWithTries(
		func() error {
			ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
			defer cancel()

			pool, err = pgxpool.New(ctx, dsn)
			if err != nil {
				return err
			}

			return nil
		},
		3,
		5*time.Second,
	)

	if err != nil {
		log.Fatal("error do with tries postgresql")
	}

	return pool, nil
}
